package kz.zhanbolat.schedule.repository;

import kz.zhanbolat.schedule.entity.Class;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface ClassRepository extends CrudRepository<Class, Long> {
    @Query(value = "select c.id, c.name, c.place, c.date_time, c.lecturer_id, c.type_id from class c " +
            "inner join class_student cs on cs.student_id = ?1 and cs.class_id = c.id " +
            "where c.date_time between ?2 and ?3", nativeQuery = true)
    List<Class> findAllByStudentIdAndTimestampBetween(Long studentId, LocalDateTime from, LocalDateTime to);

    List<Class> findAllByLecturerIdAndTimestampBetween(Long lecturerId, LocalDateTime from, LocalDateTime to);
}
