package kz.zhanbolat.schedule.service;

import kz.zhanbolat.schedule.entity.Class;
import kz.zhanbolat.schedule.repository.ClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

@Service
public class ClassServiceImpl implements ClassService {
    private static final long DEFAULT_DAYS_SHIFT = 7;
    @Autowired
    private ClassRepository classRepository;

    @Override
    public List<Class> getClassesByDateRangeAndStudentId(LocalDate from, LocalDate to, Long studentId) {
        Objects.requireNonNull(from, "From cannot be null.");
        Objects.requireNonNull(studentId, "Student id cannot be null.");

        if (Objects.isNull(to)) {
            return classRepository.findAllByStudentIdAndTimestampBetween(studentId,
                    LocalDateTime.of(from, LocalTime.MIN),
                    LocalDateTime.of(from.plusDays(DEFAULT_DAYS_SHIFT), LocalTime.MIN));
        }

        return classRepository.findAllByStudentIdAndTimestampBetween(studentId, LocalDateTime.of(from, LocalTime.MIN),
                LocalDateTime.of(to, LocalTime.MIN));
    }

    @Override
    public List<Class> getClassesByLecturerIdAndDateRange(Long lecturerId, LocalDate from, LocalDate to) {
        Objects.requireNonNull(from, "From cannot be null.");
        Objects.requireNonNull(lecturerId, "Lecturer id cannot be null.");

        if (Objects.isNull(to)) {
            return classRepository.findAllByLecturerIdAndTimestampBetween(lecturerId,
                    LocalDateTime.of(from, LocalTime.MIN),
                    LocalDateTime.of(from.plusDays(DEFAULT_DAYS_SHIFT), LocalTime.MIN));
        }

        return classRepository.findAllByLecturerIdAndTimestampBetween(lecturerId, LocalDateTime.of(from, LocalTime.MIN),
                LocalDateTime.of(to, LocalTime.MIN));
    }
}
