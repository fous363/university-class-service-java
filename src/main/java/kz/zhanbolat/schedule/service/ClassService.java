package kz.zhanbolat.schedule.service;

import kz.zhanbolat.schedule.entity.Class;

import java.time.LocalDate;
import java.util.List;

public interface ClassService {
    List<Class> getClassesByDateRangeAndStudentId(LocalDate from, LocalDate to, Long studentId);

    List<Class> getClassesByLecturerIdAndDateRange(Long lecturerId, LocalDate from, LocalDate to);
}
