package kz.zhanbolat.schedule.controller;

import kz.zhanbolat.schedule.controller.dto.ErrorResponse;
import kz.zhanbolat.schedule.entity.Class;
import kz.zhanbolat.schedule.service.ClassService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/class")
public class ClassController {
    private static final Logger logger = LoggerFactory.getLogger(ClassController.class);
    @Autowired
    private ClassService classService;

    @GetMapping("/student/{studentId}")
    public List<Class> getClassesByDateRangeAndStudentId(@PathVariable("studentId") Long studentId,
                                  @RequestParam(name = "from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                  @RequestParam(name = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        return classService.getClassesByDateRangeAndStudentId(from, to, studentId);
    }

    @GetMapping("/lecturer/{lecturerId}")
    public List<Class> getClassesByLecturerIdAndDateRange(@PathVariable("lecturerId") Long lecturerId,
                                                          @RequestParam(name = "from") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
                                                          @RequestParam(name = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        return classService.getClassesByLecturerIdAndDateRange(lecturerId, from, to);
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(Exception e) {
        logger.error("Caught exception", e);
        return new ErrorResponse(e.getMessage());
    }
}
