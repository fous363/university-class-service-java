create table if not exists class_type(
    id smallserial primary key,
    name varchar(100) not null
);

create table if not exists class(
    id bigserial primary key,
    name varchar(120) not null,
    place varchar(120) not null,
    date_time timestamp not null,
    lecturer_id bigint not null,
    type_id int not null,
    foreign key (type_id) references class_type(id)
);