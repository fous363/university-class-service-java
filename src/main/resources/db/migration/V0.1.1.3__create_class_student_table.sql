create table if not exists class_student(
    class_id bigint not null,
    student_id bigint not null,
    foreign key (class_id) references class(id)
);