insert into class(name, place, date_time, lecturer_id, type_id) values ('TEST CLASS', 'TEST', current_timestamp, 1, 1);
insert into class_student(class_id, student_id) values (1, 1);

insert into class(name, place, date_time, lecturer_id, type_id) values ('TEST CLASS 6 DAYS FROM NOW', 'TEST', current_timestamp + interval '6 day', 1, 1);
insert into class_student(class_id, student_id) values (2, 1);

insert into class(name, place, date_time, lecturer_id, type_id) values ('TEST CLASS 2 DAYS FROM NOW', 'TEST', current_timestamp + interval '2 day', 1, 1);
insert into class_student(class_id, student_id) values (3, 1);