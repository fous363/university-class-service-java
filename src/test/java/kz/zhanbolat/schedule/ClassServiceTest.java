package kz.zhanbolat.schedule;

import kz.zhanbolat.schedule.entity.Class;
import kz.zhanbolat.schedule.service.ClassService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static kz.zhanbolat.schedule.TestConstant.LECTURER_ID;
import static kz.zhanbolat.schedule.TestConstant.STUDENT_ID;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class ClassServiceTest {
    @Autowired
    private ClassService classService;

    @Test
    public void givenNullForFromAndStudentId_whenGetClassesByDateRangeAndStudentId_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> classService.getClassesByDateRangeAndStudentId(null, null, null));
            assertThrows(Exception.class, () -> classService.getClassesByDateRangeAndStudentId(null, LocalDate.now(), STUDENT_ID));
            assertThrows(Exception.class, () -> classService.getClassesByDateRangeAndStudentId(null, LocalDate.now(), null));
            assertThrows(Exception.class, () -> classService.getClassesByDateRangeAndStudentId(null, null, STUDENT_ID));
            assertThrows(Exception.class, () -> classService.getClassesByDateRangeAndStudentId(LocalDate.now(), null, null));
            assertThrows(Exception.class, () -> classService.getClassesByDateRangeAndStudentId(LocalDate.now(), LocalDate.now(), null));
        });
    }

    @Test
    public void givenFromAndStudentId_whenGetClassesByDateRangeAndStudentId_thenReturnListForNext7days() {
        LocalDate now = LocalDate.now();
        List<Class> classes = classService.getClassesByDateRangeAndStudentId(now, null, STUDENT_ID);

        assertNotNull(classes);
        assertFalse(classes.isEmpty());
        assertTrue(classes.stream().noneMatch(classSchedule -> classSchedule.getTimestamp().isBefore(LocalDateTime.of(now, LocalTime.MIN)) ||
                classSchedule.getTimestamp().isAfter(LocalDateTime.of(now.plusDays(7), LocalTime.MIN))));
    }

    @Test
    public void givenFromAndToAndStudentId_whenGetClassesByDateRangeAndStudentId_thenReturnListForThisRange() {
        LocalDate now = LocalDate.now();
        LocalDate nextDate = LocalDate.now().plusDays(1);
        List<Class> classes = classService.getClassesByDateRangeAndStudentId(now, nextDate, STUDENT_ID);

        assertNotNull(classes);
        assertFalse(classes.isEmpty());
        assertTrue(classes.stream().noneMatch(classSchedule -> classSchedule.getTimestamp().isBefore(LocalDateTime.of(now, LocalTime.MIN))
                || classSchedule.getTimestamp().isAfter(LocalDateTime.of(nextDate, LocalTime.MIN))));
    }

    @Test
    public void givenNullForFromAndLecturerId_whenGetClassesByLecturerIdAndDateRange_thenThrowException() {
       assertAll(() -> {
           assertThrows(Exception.class, () -> classService.getClassesByLecturerIdAndDateRange(null, null, null));
           assertThrows(Exception.class, () -> classService.getClassesByLecturerIdAndDateRange(null, LocalDate.now(), null));
           assertThrows(Exception.class, () -> classService.getClassesByLecturerIdAndDateRange(null, LocalDate.now(), LocalDate.now()));
           assertThrows(Exception.class, () -> classService.getClassesByLecturerIdAndDateRange(LECTURER_ID, null, null));
           assertThrows(Exception.class, () -> classService.getClassesByLecturerIdAndDateRange(LECTURER_ID, null, LocalDate.now()));
           assertThrows(Exception.class, () -> classService.getClassesByLecturerIdAndDateRange(null, null, LocalDate.now()));
       });
    }

    @Test
    public void givenFromAndLecturerId_whenGetClassesByLecturerIdAndDateRange_thenReturnList() {
        LocalDate now = LocalDate.now();
        List<Class> classes = classService.getClassesByLecturerIdAndDateRange(LECTURER_ID, now, null);

        assertNotNull(classes);
        assertFalse(classes.isEmpty());
        assertTrue(classes.stream().noneMatch(classSchedule -> classSchedule.getTimestamp().isBefore(LocalDateTime.of(now, LocalTime.MIN)) ||
                classSchedule.getTimestamp().isAfter(LocalDateTime.of(now.plusDays(7), LocalTime.MIN))));
    }

    @Test
    public void givenFromAndToAndLecturerId_whenGetClassesByLecturerIdAndDateRange_thenReturnList() {
        LocalDate now = LocalDate.now();
        LocalDate nextDate = LocalDate.now().plusDays(1);
        List<Class> classes = classService.getClassesByLecturerIdAndDateRange(LECTURER_ID, now, nextDate);

        assertNotNull(classes);
        assertFalse(classes.isEmpty());
        assertTrue(classes.stream().noneMatch(classSchedule -> classSchedule.getTimestamp().isBefore(LocalDateTime.of(now, LocalTime.MIN)) ||
                classSchedule.getTimestamp().isAfter(LocalDateTime.of(nextDate, LocalTime.MIN))));
    }
}
