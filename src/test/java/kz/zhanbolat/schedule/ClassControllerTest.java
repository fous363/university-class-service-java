package kz.zhanbolat.schedule;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static kz.zhanbolat.schedule.TestConstant.LECTURER_ID;
import static kz.zhanbolat.schedule.TestConstant.STUDENT_ID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ClassControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = "STUDENT")
    public void givenFromAndStudentId_whenGetClassesByDateRangeAndStudentId_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/class/student/" + STUDENT_ID + "?from=" + LocalDate.now()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "STUDENT")
    public void givenFromAndToAndStudentId_whenGetClassesByDateRangeAndStudentId_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/class/student/" + STUDENT_ID + "?from=" + LocalDate.now() + "&to=" + LocalDate.now().plusDays(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "LECTURER")
    public void givenFromAndLecturerId_whenGetClassesByLecturerIdAndDateRange_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/class/lecturer/" + LECTURER_ID + "?from=" + LocalDate.now()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "LECTURER")
    public void givenFromAndToAndLecturerId_whenGetClassesByLecturerIdAndDateRange_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/class/lecturer/" + LECTURER_ID + "?from=" + LocalDate.now() + "&to=" + LocalDate.now().plusDays(1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }
}
